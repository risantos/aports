# Contributor: Galen Abell <galen@galenabell.com>
# Contributor: Maxim Karasev <begs@disroot.org>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=i3status-rust
pkgver=0.21.9
pkgrel=0
pkgdesc="i3status replacement in Rust"
url="https://github.com/greshake/i3status-rust"
arch="all !s390x !riscv64" # limited by cargo
license="GPL-3.0-only"
makedepends="rust cargo dbus-dev openssl-dev libpulse lm-sensors-dev"
options="net !check" # no tests
provides="i3status-rs"
subpackages="$pkgname-doc"
source="https://github.com/greshake/i3status-rust/archive/v$pkgver/i3status-rust-$pkgver.tar.gz"

build() {
	cargo build --release --verbose --locked
}

package() {
	install -Dm755 target/release/i3status-rs "$pkgdir"/usr/bin/i3status-rs

	install -Dm644 man/i3status-rs.1 "$pkgdir"/usr/share/man/man1/i3status-rs.1

	install -Dm644 -t "$pkgdir"/usr/share/i3status-rust/themes files/themes/*
	install -Dm644 -t "$pkgdir"/usr/share/i3status-rust/icons files/icons/*
}

sha512sums="
dae2dcb56fc95f724e674747f1b4d3dd7ead7ba83e0a03cecd2efdf9ce91fe64db671ccd6bbf0eb13449d2b0f37bb64a4353f9fdfe0b89ad8fe4733332e02eb9  i3status-rust-0.21.9.tar.gz
"
